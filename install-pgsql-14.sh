#!/bin/bash
# Setting Timezone Asia/Jakarta
sudo ln -snf /usr/share/zoneinfo/Asia/Jakarta /etc/localtime && sudo echo Asia/Jakarta > /etc/timezone

# Install Postgresql 14
sudo apt-get update -y
sudo apt-get upgrade -y
sudo apt-get install -y gnupg2 wget nano
sudo apt-get update -y
sudo apt-get install postgresql-14 -y
sudo systemctl enable postgresql
sudo systemctl status postgresql
